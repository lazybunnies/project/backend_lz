import { Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}
  // constructor(
  //   private usersService: UsersService,
  //   private jwtService: JwtService,
  // ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = this.dataSource.query(
      `SELECT * FROM user WHERE username = '${username}' AND password = '${pass}'`,
    );
    if (user) {
      return user;
    }
    return null;
  }

  // const user = await this.usersService.findOneByUserName(username);
  // const isMatch = await bcrypt.compare(pass, user.password);
  // if (user && isMatch) {
  //   const { password, ...result } = user;
  //   return result;
  // }
  // return null;

  async login(user: User) {
    const payload = { username: user.username, sub: user.id };
    return {
      user,
      // access_token: this.jwtService.sign(payload),
    };
  }
}
// import { Injectable } from '@nestjs/common';
// import { JwtService } from '@nestjs/jwt';
// import { User } from 'src/users/entities/user.entity';
// import { UsersService } from '../users/users.service';
// import * as bcrypt from 'bcrypt';

// @Injectable()
// export class AuthService {
//   constructor(
//     private usersService: UsersService,
//     private jwtService: JwtService,
//   ) {}

//   async validateUser(username: string, pass: string): Promise<any> {
//     const user = await this.usersService.findOneByUserName(username);
//     const isMatch = await bcrypt.compare(pass, user.password);
//     if (user && isMatch) {
//       const { password, ...result } = user;
//       return result;
//     }
//     return null;
//   }

//   async login(user: User) {
//     const payload = { username: user.username, sub: user.id };
//     return {
//       user,
//       access_token: this.jwtService.sign(payload),
//     };
//   }
// }
